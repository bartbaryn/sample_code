import { INestApplicationContext } from '@nestjs/common';
import { Handler, Request, Response } from 'express';
import expressWinston from 'express-winston';
import winstonTransport from 'winston-transport';

import { Logger } from '../services/logger.service';

type LoggerMessage = {
  level: string;
  message: string;
  meta: string;
}

type WinstonRequest = {
  id: string;
  user: {
    id: string;
    role: string;
  }
}

class LoggerTransport extends winstonTransport {
  private readonly contextName = 'RequestHandler';

  constructor(private readonly logger: Logger) {
    super({
      silent: process.env.NODE_ENV === 'test'
    });
  }

  log(info: LoggerMessage, next: () => void): void {
    switch (info.level) {
      case 'warn':
        this.logger.warn(info.message, this.contextName, info.meta);
        break;
      case 'error':
        this.logger.error(info.message, undefined, this.contextName, info.meta);
        break;
      default:
        this.logger.log(info.message, this.contextName, info.meta);
    }
    next();
  }

  logv(info: LoggerMessage, next: () => void): void {
    this.logger.verbose(info.message, this.contextName, info.meta);
    next();
  }
}

export function winstonRequestLogger(app: INestApplicationContext): Handler {
  return expressWinston.logger({
    transports: [new LoggerTransport(app.get(Logger))],
    dynamicMeta: (req: Request) => ({
      requestId: req.body.id,
      user: {
        id: req.body.user?.id,
        role: req.body.user?.role
      }
    }),
    level: (req: Request, res: Response) => {
      const statusCode = res.statusCode;
      if (statusCode >= 500) {
        return 'error';
      }
      if (statusCode >= 400) {
        return 'warn';
      }
      return 'info';
    }
  });
}
