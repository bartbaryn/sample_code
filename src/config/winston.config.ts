import { createLogger, format, Logger as WinstonLogger } from 'winston';
import * as Transport from 'winston-transport';
import winstonDailyRotateFile from 'winston-daily-rotate-file';

const { combine, timestamp, json, uncolorize } = format;

export const fileFormat = combine(timestamp(), uncolorize(), json());

export function createWinstonLogger(name: string): WinstonLogger {
  const rotateFileOptions = {
    dirname: `./logs/${name}`,
    datePattern: 'YYYY-MM-DD,HH',
    zippedArchive: true,
    maxSize: '10m',
    silent: process.env.NODE_ENV === 'test',
    format: fileFormat,
  };

  const loggerTransports: Transport[] = [];

  if (process.env.NODE_ENV !== 'test') {
    const dailyCombinedFileTransport = new winstonDailyRotateFile({
      ...rotateFileOptions,
      filename: `winston.%DATE%.log`,
      level: 'info',
    });

    const dailyErrorFileTransport = new winstonDailyRotateFile({
      ...rotateFileOptions,
      filename: `winston.%DATE%.error.log`,
      level: 'error',
    });

    loggerTransports.push(
      dailyCombinedFileTransport,
      dailyErrorFileTransport,
    );
  }

  const logger: WinstonLogger = createLogger({
    transports: loggerTransports,
  });

  return logger;
}
