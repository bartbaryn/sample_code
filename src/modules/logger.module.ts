import {
  DynamicModule,
  Global,
  INestApplication,
  INestApplicationContext,
  INestMicroservice,
  Module,
  Provider,
} from '@nestjs/common';
import { Transport } from '@nestjs/microservices';

import { TcpLoggerInterceptor } from '../interceptors';
import { RabbitMqLoggerInterceptor } from '../interceptors/microservices/rabbitMqLogger.interceptor';
import { RedisLoggerInterceptor } from '../interceptors/microservices/redisLogger.interceptor';
import { LoggerModuleOptions } from '../interfaces/loggerModuleOptions.interface';
import { HttpRequestLoggerMiddleware } from '../middleware/httpRequestLog.middleware';
import { DEFAULT_LOGGER_CONTEXT, Logger } from '../services/logger.service';

@Global()
@Module({})
export class LoggerModule {
  private static loggerInstance: Logger;

  static register(options: LoggerModuleOptions = { context: DEFAULT_LOGGER_CONTEXT }): DynamicModule {
    const { context } = options;

    const logger = new Logger(context);

    const loggerProvider: Provider<Logger> = {
      provide: Logger,
      useValue: logger
    };

    const tcpLoggerProvider: Provider<TcpLoggerInterceptor> = {
      provide: TcpLoggerInterceptor,
      useValue: new TcpLoggerInterceptor(logger),
    };

    const redisLoggerProvider: Provider<RedisLoggerInterceptor> = {
      provide: RedisLoggerInterceptor,
      useValue: new RedisLoggerInterceptor(logger),
    };

    const rabbitMqLoggerProvider: Provider<RabbitMqLoggerInterceptor> = {
      provide: RabbitMqLoggerInterceptor,
      useValue: new RabbitMqLoggerInterceptor(logger),
    };

    const exportedProviders = [
      loggerProvider,
      tcpLoggerProvider,
      redisLoggerProvider,
      rabbitMqLoggerProvider,
    ];

    return {
      module: LoggerModule,
      providers: exportedProviders,
      exports: exportedProviders
    };
  }

  static setupApp(app: INestApplication): void {
    const logger = app.get(Logger);
    this.commonSetup(app, logger);

    app.use(
      HttpRequestLoggerMiddleware.requestId(),
      HttpRequestLoggerMiddleware.logger(app),
    );
  }

  static setupMicroservice(app: INestMicroservice, transport: Transport): void {
    const logger = app.get(Logger);
    this.commonSetup(app, logger);

    switch (transport) {
      case Transport.TCP:
        app.useGlobalInterceptors(app.get(TcpLoggerInterceptor));
        break;
      case Transport.REDIS:
        app.useGlobalInterceptors(app.get(RedisLoggerInterceptor));
        break;
      case Transport.RMQ:
        app.useGlobalInterceptors(app.get(RabbitMqLoggerInterceptor));
        break;
      default:
        break;
    }
  }

  static fatalError(message: string, error: string | any, context?: string): void {
    this.getLoggerInstance().error(`Fatal error occurred: ${message} ${error}`, error?.stack, context, { error });
    process.exit(1);
  }

  static context(): string {
    return this.getLoggerInstance().context;
  }

  private static commonSetup(app: INestApplicationContext, logger: Logger): void {
    this.loggerInstance = logger;
    app.useLogger(logger);
    LoggerModule.registerFatalErrorHooks();
  }

  private static registerFatalErrorHooks(): void {
    process.on('uncaughtException', e => {
      this.fatalError('uncaughtException', e, 'UncaughtException');
    });

    process.on('unhandledRejection', e => {
      this.fatalError('unhandledRejection', e, 'UnhandledRejection');
    });
  }

  private static getLoggerInstance(): Logger {
    if (!this.loggerInstance) {
      console.warn('LoggerModule was not set up correctly, did you forgot to call `setupApp` or `setupMicroservice`?');
      console.warn('Using new logger instance with default settings...');
      this.loggerInstance = new Logger();
    }

    return this.loggerInstance;
  }
}
