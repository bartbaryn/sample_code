import { Injectable } from '@nestjs/common';
import { RpcArgumentsHost } from '@nestjs/common/interfaces';
import { RedisContext } from '@nestjs/microservices';

import { BaseLoggerInterceptor } from './baseLogger.interceptor';

@Injectable()
export class RedisLoggerInterceptor extends BaseLoggerInterceptor {
  protected getPattern(rpcContext: RpcArgumentsHost): string {
    const redisContext = rpcContext.getContext<RedisContext>();
    return redisContext.getChannel().replace('_ack', '');
  }
}
