import { CallHandler, ExecutionContext, NestInterceptor } from '@nestjs/common';
import { RpcArgumentsHost } from '@nestjs/common/interfaces';
import { Observable, of, throwError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { v4 } from 'uuid';

import { Logger } from '../../services/logger.service';
import { redactLogMessage } from '../../utils/logRedactor';
import { nowUnixTimestamp } from '../../utils/unixTimestamp';

export abstract class BaseLoggerInterceptor implements NestInterceptor {
  constructor(private readonly logger: Logger) { }

  protected abstract getPattern(rpcContext: RpcArgumentsHost): string;

  intercept(context: ExecutionContext, next: CallHandler): Observable<Record<string, unknown>> | Promise<Observable<Record<string, unknown>>> {
    const now = Date.now();
    const rpcContext = context.switchToRpc();

    const pattern = this.getPattern(rpcContext);
    const payload = rpcContext.getData();

    const contextName = context.getClass().name;

    const id = payload?.requestMeta?.requestId || v4();

    this.logger.log(
      `Received RPC call ${pattern}`,
      contextName,
      this.redactMessage({
        message: {
          pattern: JSON.parse(pattern),
          data: payload,
        },
        requestId: id,
        timestamp: nowUnixTimestamp(),
      })
    );

    const resultObservable = next.handle().pipe(
      catchError((error, _) => {
        const messageToRedact = {
          message: {
            pattern: JSON.parse(pattern),
            data: payload
          },
          responseTime: Date.now() - now,
          requestId: id,
          timestamp: nowUnixTimestamp(),
        };
        const redactedLog = this.redactMessage(messageToRedact);

        this.logger.error(
          `Error while processing RPC call (${pattern}): ${error.toString()}`,
          error?.stack,
          contextName,
          redactedLog
        );
        return throwError(error);
      })
    );

    return resultObservable.pipe(
      tap(res => {
        const messageToRedact = {
          message: {
            pattern: JSON.parse(pattern),
            data: payload,
          },
          result: res,
          responseTime: Date.now() - now,
          requestId: id,
          timestamp: nowUnixTimestamp(),
        };

        const redactedLog = this.redactMessage(messageToRedact);
        this.logger.log(`Processed RPC call (pattern: ${pattern})`, contextName, redactedLog);

        return of(res);
      })
    );
  }

  private redactMessage(message: string | Record<string, unknown>): Record<string, unknown> {
    return redactLogMessage(message);
  }
}
