import { Injectable } from '@nestjs/common';
import { RpcArgumentsHost } from '@nestjs/common/interfaces';
import { TcpContext } from '@nestjs/microservices';

import { BaseLoggerInterceptor } from './baseLogger.interceptor';

@Injectable()
export class TcpLoggerInterceptor extends BaseLoggerInterceptor {
  protected getPattern(rpcContext: RpcArgumentsHost): string {
    const tcpContext = rpcContext.getContext<TcpContext>();

    return tcpContext.getPattern();
  }
}
