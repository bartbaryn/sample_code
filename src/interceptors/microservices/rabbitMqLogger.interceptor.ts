import { Injectable } from '@nestjs/common';
import { RpcArgumentsHost } from '@nestjs/common/interfaces';
import { RmqContext } from '@nestjs/microservices';

import { BaseLoggerInterceptor } from './baseLogger.interceptor';

@Injectable()
export class RabbitMqLoggerInterceptor extends BaseLoggerInterceptor {
  protected getPattern(rpcContext: RpcArgumentsHost): string {
    const context = rpcContext.getContext<RmqContext>();
    return context.getPattern();
  }
}
