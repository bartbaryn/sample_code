import moment from 'moment';

export function nowUnixTimestamp(): number {
  return moment().unix();
}
