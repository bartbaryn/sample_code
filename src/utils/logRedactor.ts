const protectedKeys = [
  'password',
  'accessToken'
];

const REDACTED = '** REDACTED **';

function logReplacer(key: string, value: unknown): unknown {
  if (protectedKeys.includes(key)) {
    return REDACTED;
  }
  return value;
}

export function redactLogMessage(message: unknown): Record<string, unknown> {
  return JSON.parse(JSON.stringify(message, logReplacer));
}
