import { INestApplicationContext } from '@nestjs/common';
import { Handler } from 'express';
import expressRequestId from 'express-request-id';

import { winstonRequestLogger } from '../config/winstonExpress.config';

export class HttpRequestLoggerMiddleware {
  static requestId(): Handler {
    return expressRequestId();
  }

  static logger(app: INestApplicationContext): Handler {
    return winstonRequestLogger(app);
  }
}
