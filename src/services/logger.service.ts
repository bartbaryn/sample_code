import { Injectable, Logger as NestLogger, LoggerService } from '@nestjs/common';
import { Logger as WinstonLogger } from 'winston';

import { createWinstonLogger } from '../config/winston.config';
import { redactLogMessage } from '../utils/logRedactor';

export const LOGGER_SERVICE_TOKEN = 'sample-logger';
export const DEFAULT_LOGGER_CONTEXT = 'SampleLogger';

const defaultMeta = {
  application: process.env.APP_NAME,
  environment: process.env.NODE_ENV
};

@Injectable()
export class Logger extends NestLogger implements LoggerService {
  private winstonLogger: WinstonLogger;

  constructor(readonly context: string = DEFAULT_LOGGER_CONTEXT) {
    super(context, true);
    this.winstonLogger = createWinstonLogger(context);
  }

  log(message: string, context?: string, meta?: string | any): void {
    this.doAsync(() => {
      super.log(this.prepareMessageForConsole(message, meta), context);
      this.winstonLogger.info({
        message,
        context,
        meta: this.convertMetaToObject(meta),
        ...defaultMeta
      });
    });
  }

  error(message: string, trace?: string, context?: string, meta?: string | any): void {
    this.doAsync(() => {
      super.error(this.prepareMessageForConsole(message, meta), trace, context);

      this.winstonLogger.error({
        message,
        context,
        trace,
        meta: this.convertMetaToObject(meta),
        ...defaultMeta
      });
    });
  }

  warn(message: string, context?: string, meta?: string | any): void {
    this.doAsync(() => {
      super.warn(this.prepareMessageForConsole(message, meta), context);

      this.winstonLogger.warn({
        message,
        context,
        meta: this.convertMetaToObject(meta),
        ...defaultMeta
      });
    });
  }

  debug(message: string, context?: string, meta?: string | any): void {
    this.doAsync(() => {
      super.debug(this.prepareMessageForConsole(message, meta), context);
      this.winstonLogger.debug({
        message,
        context,
        meta: this.convertMetaToObject(meta),
        ...defaultMeta,
      });
    });
  }

  verbose(message: string, context?: string, meta?: string | any): void {
    this.doAsync(() => {
      super.verbose(this.prepareMessageForConsole(message, meta), context);
      this.winstonLogger.verbose({
        message,
        context,
        meta: this.convertMetaToObject(meta),
        ...defaultMeta,
      });
    });
  }

  private prepareMessageForConsole(message: string, meta?: string | any): string | any {
    if (typeof message === 'string') {
      if (typeof meta === 'boolean' || typeof meta === 'undefined') {
        return message;
      }
      const redactedMeta = redactLogMessage(meta);
      return `${message}\n${JSON.stringify(redactedMeta, null, 2)}`;
    }
    return redactLogMessage({
      message,
      meta: this.convertMetaToObject(meta),
    });
  }

  private doAsync(action: () => void): Promise<void> {
    return new Promise<void>((resolve, reject) => {
      try {
        action();
        return resolve();
      } catch (error) {
        return reject(error);
      }
    }).catch(err => console.log('Async logging error', err));
  }

  private convertMetaToObject(meta: string | any): Record<string, unknown> {
    if (!meta) {
      return undefined;
    }

    if (typeof meta === 'object') {
      return meta;
    }
    return { meta: JSON.stringify(meta) };
  }
}
